from os import path, mkdir
from termcolor import colored
from socket import socket, AF_INET, SOCK_STREAM
from logging import info, warning

def create_directory(directory_name="result_attack"):
    try:
        if not path.isdir(directory_name):
            mkdir(directory_name)
    except OSError as error: 
        print(error)  


def show_password_file(path_file):
    if path.exists(path_file):
        with open(path_file, 'r') as protocol_file:
            result = protocol_file.read()
        print(colored(f"Contents of  {path_file} file", "blue"))
        print(colored(result, "green"))


def read_dictionary_file(logfile_filename="/usr/share/wordlists/metasploit/unix_users.txt"):
    if path.exists(logfile_filename):
        with open(logfile_filename, 'r') as logfile:
           log = [logf.replace("\n", '') for logf in logfile]
        return log        
    else:
        with open(logfile_filename, 'w') as logfile:
            pass


def port_available(ip_address, number_port=22):
    s = socket(AF_INET, SOCK_STREAM)
    port = s.connect_ex((ip_address, number_port))
    if port == 0:
        s.close()
        info(f"[+] Port {number_port} is open")
        return True
    else:
        s.close()
        warning(f"[-] Port {number_port} is not open")
        return False

