# MajorAttack

MajorAttack is script for performing dictionary and brute force attacks on ssh, ftp, sftp, telnet, smtp services

![graphic1](./img/1.png)

![graphic2](./img/2.png)

## Table of Contents

- [MajorAttack](#majorattack)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Instalation](#instalation)
  - [Running](#running)
  - [Technologies](#technologies)
  - [Authors](#authors)
  - [License](#license)

## Features

- Attack dictionary
- Attack brute force
- Saving found logins and passwords into a file together with the address

## Instalation

Before running the script for the first time, run the commands

```python3
 pip3 install -r requirements.txt
```

```sh
  sudo apt install python3-termcolor
```

## Running

```python3
 python3 major_attack.py [option] 
```

options:

- -d, --dict   ->   dictionary attack
- -b, --brute  ->  brute force attack

## Technologies

Project is created with:

- python 3.9.7
- paramiko 2.10.2
- smtplib
- pyfiglet
- colored
- termcolor

## Authors

***
__goodbit22__ --> 'https://gitlab.com/users/goodbit22'
***

## License

   Apache License Version 2.0
